import { Component, OnInit } from '@angular/core';
import { Planet } from 'src/app/interfaces/Planet';
import { PlanetsService } from '../planets.service';

@Component({
  selector: 'app-detail-planet',
  templateUrl: './detail-planet.component.html',
  styleUrls: ['./detail-planet.component.css']
})
export class DetailPlanetComponent implements OnInit {
  private planet: Planet;

  constructor(private http: PlanetsService) { }

  ngOnInit() {
    console.log(this)
  }

  getImage(){
    
  }

  getPlanet(id: Number){
    this.http.getPlanetById(id)
      .subscribe( (res:any) => {
        console.log(res);
        
      }, (error:any) => {

      });
  }

}
