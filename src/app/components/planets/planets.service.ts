import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, retry, catchError, finalize, filter } from 'rxjs/operators';
import { of } from 'rxjs';
import { Planet } from '../../interfaces/Planet';
 

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  urlApi:string = 'https://swapi.co/api/planets';
  private format:string = '?format=json';

  constructor(private http: HttpClient) { }

  getPlanets(){
    return this.http.get(this.urlApi.concat(this.format));
  }
  
  getPlanetById(id: Number){
    return this.http.get(this.urlApi.concat("/", id.toString() , this.format));
  }
}
