import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetsRoutingModule} from './planets-routing.module';
import { ListPlanetsComponent } from './list-planets/list-planets.component';
import { DetailPlanetComponent } from './detail-planet/detail-planet.component';
import { PlanetsService } from './planets.service';

@NgModule({
  imports: [
    CommonModule,
    PlanetsRoutingModule
  ],
  declarations: [
    ListPlanetsComponent,
    DetailPlanetComponent
  ],
  providers: [
    PlanetsService
  ]
})
export class PlanetsModule { }
