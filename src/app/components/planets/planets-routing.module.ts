import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailPlanetComponent } from './detail-planet/detail-planet.component';
import { ListPlanetsComponent } from './list-planets/list-planets.component';



const routes: Routes = [
  { path: '', pathMatch:'full', component: ListPlanetsComponent},
  { path: 'planet/:id', component: DetailPlanetComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanetsRoutingModule { }