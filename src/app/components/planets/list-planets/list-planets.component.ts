import { Component, OnInit, ɵConsole } from '@angular/core';
import { Planet } from 'src/app/interfaces/Planet';
import { PlanetsService } from '../planets.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-list-planets',
  templateUrl: './list-planets.component.html',
  styleUrls: ['./list-planets.component.css']
})
export class ListPlanetsComponent implements OnInit {

  private listPlanet:Planet[] = [];

  private ngUnsubscribe = new Subject<void>();

  constructor(private http:PlanetsService) { }

  ngOnInit() {
    this.getPlanets();
  }

  getPlanets(){
    console.log("[getPlanet()]");
    this.http.getPlanets()
      .subscribe( (res:any) => {
        res.results.forEach(element => {
          let planet = {
            id: element.url.match(/\d+/)[0],
            name: element.name,
            climate: element.climate,
            terrain: element.terrain,
            population: element.population,
            rotation_period: element.rotation_period,
            gravity: element.gravity,
            url: element.url
          };
          this.addPlanet(planet);
        });
        console.log(this.listPlanet);
        this.ngUnsubscribe;
      },
      error => {
        console.log(error);
      });
  }

  addPlanet(planet: Planet){
    this.listPlanet.push(planet);
  }

  ngOnDestroy(){
    console.log("2 Estado unsuscribe [ngOnDestroy]: ", JSON.stringify(this.ngUnsubscribe))
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    // Se lanza al cambiar de hijo y el close pasa a true (ver consola)
    console.log("3 Estado unsuscribe [ngOnDestroy Al cambiar hijo]:", JSON.stringify(this.ngUnsubscribe))
  }

}
