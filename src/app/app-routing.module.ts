import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/shared/error/error.component';


const routes: Routes = [
  { path: 'home', component:HomeComponent},
  { path: 'planets', loadChildren: () => import(`./components/planets/planets.module`).then(m => m.PlanetsModule)},
  { path: 'error', component: ErrorComponent},
  { path: '', pathMatch: 'full', redirectTo:'/home'},
  { path: '**', pathMatch: 'full', redirectTo:'/error'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

