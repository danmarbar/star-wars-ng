export interface Planet {
    id: Number;
    name: String;
    climate: String;
    terrain:String;
    population: Number;
    rotation_period: Number;
    gravity: Number;
    url: String;
}
